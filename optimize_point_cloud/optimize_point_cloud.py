def point_sub(pt1, pt2):
	return geom.Point3(pt1.x - pt2.x, pt1.y - pt2.y, pt1.z - pt2.z)

def dot_product(pt1, pt2):
	return pt1.x * pt2.x + pt1.y * pt2.y + pt1.z * pt2.z

def vector_length(v):
	return math.sqrt(dot_product(v, v))

def get_size(occurrence):
	aabb = scene.getAABB([occurrence])
	return vector_length(point_sub(aabb.high, aabb.low))

def optimize_point_cloud(root, size):
	algo.voxelizePointClouds([root], size/10)
	for occurrence in scene.getPartOccurrences(root):
		scene.select([occurrence])
		generateproxy.proxyMeshWithTextures(200, ["Yes", pxz.generateproxy.BakeOptions(1024, 1, pxz.generateproxy.BakeMaps(True, False, False, False, False, False, False, False))], False, 2)
		scene.clearSelection()

def main(root):
	size = get_size(root)
	optimize_point_cloud(root, size)

main(2)