# Get Metadata Value (Recursively)

Look for a property given a name.  
If not found on a given occurrence, it will search for this property on each of its parents until it is found.  
Returns the value of the property if found, otherwise returns "None".