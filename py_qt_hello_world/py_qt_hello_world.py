from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QVBoxLayout, QWidget, QSlider, QPushButton

main_occurrence = None

def slider_value_changed(value):
	global main_occurrence
	core.setProperty(main_occurrence, 'Transform', str(geom.fromTRS(pxz.geom.Point3(0,0,0), pxz.geom.Point3(0,0,0), pxz.geom.Point3(1,1+value,1))))
	view.fit([main_occurrence])

def create_text_occurrence(text):
	occurrence = scene.createOccurrenceFromText(text)
	scene.setParent(occurrence, scene.getRoot(), False, 0)
	view.fit([occurrence])
	view.setMainViewerProperty('DoubleSided', 'True')
	return occurrence

def button_clicked():
	global main_occurrence
	scene.applyTransformation(main_occurrence, geom.fromTRS(pxz.geom.Point3(0,0,0), pxz.geom.Point3(0,3.14,0), pxz.geom.Point3(1,1,1)))
	view.fit([main_occurrence])

def main():
	global main_occurrence
	main_occurrence = create_text_occurrence('Hello World')
	
	layout = QVBoxLayout()
	main_widget = QWidget()
	
	slider_widget = QSlider(QtCore.Qt.Horizontal)
	slider_widget.setMinimum(0)
	slider_widget.setMaximum(5)
	slider_widget.valueChanged.connect(slider_value_changed)
	layout.addWidget(slider_widget)

	occurrence_visibility_widget = coregui.getPropertyWidget(main_occurrence, 'Visible')
	layout.addWidget(occurrence_visibility_widget)
	
	push_button_widget = QPushButton('Hello World')
	push_button_widget.clicked.connect(button_clicked)
	layout.addWidget(push_button_widget)

	main_widget.setLayout(layout)
	coregui.dockWidget(main_widget, 'Hello World')
	coregui.applyApplicationStyle(main_widget)

main()